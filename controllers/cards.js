const Card = require("../models/card");
const { CardType } = require('../models/card');

function list(req, res, next) {
  const typeCard = req.params.typeCard;
  const userID = req.params.userID;
  let page = req.params.page ? req.params.page : 1;

  var params = new Object();

  if (typeCard) {
    params['_type_card'] = typeCard;
  }

  if (userID) {
    params['_user_id'] = userID;
  }

  Card.paginate(params, { page: page, limit: 10 })
    .then((obj) =>
      res.status(200).json({
        "message": "Tarjetas cargadas correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Error al cargar tarjetas",
        obj: error,
      })
    );
}


function create(req, res, next) {
  let curp = req.body.curp;
  let notes = req.body.notes;
  let state = req.body.state;
  let title = req.body.title;
  let serie = req.body.serie;
  let userID = req.body.userID;
  let section = req.body.section;
  let carModel = req.body.carModel;
  let typeCard = req.body.typeCard;
  let validityDate = req.body.validityDate;
  let cardNumber = req.body.cardNumber;
  let accountName = req.body.accountName;
  let municipality = req.body.municipality;

  let card = new Card({
    curp: curp,
    notes: notes,
    state: state,
    serie: serie,
    title: title,
    userID: userID,
    section: section,
    typeCard: typeCard,
    validityDate: validityDate,
    carModel: carModel,
    cardNumber: cardNumber,
    accountName: accountName,
    municipality: municipality
  });

  card.save().then(obj => res.status(200).json({
    message: "Tarjeta guardada",
    objs: obj
  })).catch(error => res.status(500).json({
    message: "La tarjeta no se pudo guardar",
    objs: error
  }));
}

function index(req, res, next) {
  const id = req.params.id;

  Card.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": "Tarjeta cargada correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Error al cargar la tarjeta",
        obj: error,
      })
    );
}

function update(req, res, next) {
  let id = req.params.id;
  let curp = req.body.curp;
  let notes = req.body.notes;
  let serie = req.body.serie;
  let state = req.body.state;
  let title = req.body.title;
  let section = req.body.section;
  let carModel = req.body.carModel;
  let validity = req.body.validity;
  let cardNumber = req.body.cardNumber;
  let accountName = req.body.accountName;
  let municipality = req.body.municipality;

  let card = new Object();

  if (accountName) {
    card._account_name = accountName;
  }
  if (notes) {
    card._notes = notes;
  }
  if (cardNumber) {
    card._card_number = cardNumber;
  }
  if (carModel) {
    card._car_model = carModel;
  }
  if (serie) {
    card._serie = serie;
  }
  if (curp) {
    card._curp = curp;
  }
  if (state) {
    card._state = state;
  }
  if (title) {
    card._title = title;
  }
  if (section) {
    card._section = section;
  }
  if (municipality) {
    card._municipality = municipality;
  }
  if (validity) {
    card._validity_date = validity;
  }

  Card.findOneAndUpdate({ _id: id }, card)
    .then((obj) =>
      res.status(200).json({
        "message": "Tarjeta actualizada correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Tarjeta no encontrada",
        obj: error,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;

  Card.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": "Tarjeta eliminada correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Tarjeta no encontrada",
        obj: error,
      })
    );
}

module.exports = {
  list,
  create,
  index,
  update,
  destroy,
};