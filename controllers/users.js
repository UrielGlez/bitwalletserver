const User = require("../models/user");

function list(req, res, next) {
  let page = req.params.page ? req.params.page : 1;

  User.paginate({}, {page: page, limit: 10})
    .then((obj) =>
      res.status(200).json({
        "message": "Usuarios cargados correctamente", 
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Error al cargar usuarios",
        obj: error,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  User.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": "Usuario cargado correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Error al cargar al usuario",
        obj: error,
      })
    );
}

function replace(req, res, next) {
  let id = req.params.id;

  let email = req.body.email ? req.body.email : "";
  let name = req.body.firstName ? req.body.firstName : "";
  let lastName = req.body.lastName ? req.body.lastName : "";
  let password = req.body.password ? req.body.password : "";
  let pin = req.body.pin ? req.body.pin : "";

  let user = new Object({
    _email: email,
    _pin: pin,
    _first_name : name,
    _last_name: lastName,
    _password: password,
  });

  User.findOneAndReplace({ _id: id }, user)
    .then((obj) =>
      res.status(200).json({
        "message": "Usuario actualizado correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Usuario no encontrado",
        obj: error,
      })
    );
}

function update(req, res, next) {
  let id = req.params.id;
  let email = req.body.email;
  let name = req.body.firstName;
  let lastName = req.body.lastName;
  let password = req.body.password;
  let pin = req.body.pin;

  let user = new Object();

  if (pin) {
    user._pin = pin;
  }
  if (email) {
    user._email = email;
  }
  if (name) {
    user._first_name = name;
  }
  if (lastName) {
    user._last_name = lastName;
  }
  if (password) {
    user._password = password;
  }

  User.findOneAndUpdate({ _id: id }, user)
    .then((obj) =>
      res.status(200).json({
        "message": "Usuario actualizado correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Usuario no encontrado",
        obj: error,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  
  User.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        "message": "Usuario eliminado correctamente",
        objs: obj,
      })
    )
    .catch((error) =>
      res.status(500).json({
        "message": "Usuario no encontrado",
        obj: error,
      })
    );
}

module.exports = {
  list,
  index,
  replace,
  update,
  destroy,
};
