const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const CardType = Object.freeze({
    CREDIT_DEBIT_CARD: 'CREDIT_DEBIT_CARD',
    DRIVER_LICENSE: 'DRIVER_LICENSE',
    LICENSE_PLATE: 'LICENSE_PLATE',
    GIFT_CARD: 'GIFT_CARD',
    ID_CARD: 'ID_CARD'
});

const schema = mongoose.Schema({
    _type_card: {
        type: String,
        enum: Object.values(CardType),
    },
    _user_id: String,
    _notes: String,
    _card_number: String,
    _serie: String,
    _account_name: String,
    _car_model: String,
    _title: String,
    _curp: String,
    _state: String,
    _municipality: String,
    _section: String,
    _validity_date: Date
});

Object.assign(schema.statics, {
    CardType,
});

class Card {

    constructor(carModel, notes, accountName, serie, cardNumber, typeCard, title, curp, state, municipality, section, validityDate, userID) {
        this._municipality = municipality;
        this._account_name = accountName;
        this._card_number = cardNumber;
        this._validity_date = validityDate;
        this._car_model = carModel;
        this._type_card = typeCard;
        this._section = section;
        this._user_id = userID;
        this._state = state;
        this._title = title;
        this._serie = serie;
        this._notes = notes;
        this._curp = curp;
    }

    get carModel() {
        return this._car_model;
    }

    set carModel(v) {
        this._car_model = v;
    }

    get notes() {
        return this._notes;
    }

    set notes(v) {
        this._notes = v;
    }

    get accountName() {
        return this._account_name;
    }

    set accountName(v) {
        this._account_name = v;
    }

    get serie() {
        return this._serie;
    }

    set serie(v) {
        this._serie = v;
    }

    get cardNumber() {
        return this._card_number;
    }

    set cardNumber(v) {
        this._card_number = v;
    }

    get userID() {
        return this._user_id;
    }

    set userID(v) {
        this._user_id = v;
    }

    get typeCard() {
        return this._type_card;
    }

    set typeCard(v) {
        this._type_card = v;
    }

    get section() {
        return this._section;
    }

    set section(v) {
        this._section = v;
    }

    get validityDate() {
        return this._validity_date;
    }

    set validityDate(v) {
        this._validity_date = v;
    }

    get title() {
        return this._title;
    }

    set title(v) {
        this._title = v;
    }

    get curp() {
        return this._curp;
    }

    set curp(v) {
        this._curp = v;
    }

    get state() {
        return this._state;
    }

    set state(v) {
        this._state = v;
    }

    get municipality() {
        return this._municipality;
    }

    set municipality(v) {
        this._municipality = v;
    }
}

schema.plugin(mongoosePaginate)
schema.loadClass(Card);
module.exports = mongoose.model('Card', schema);
