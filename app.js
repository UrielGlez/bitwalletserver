var createError = require('http-errors');
var express = require('express');
const log4js = require("log4js");
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const expressJwt = require('express-jwt');
var cors = require('cors');
const config = require('config');

var usersRouter = require('./routes/users');
var authenticationRouter = require('./routes/authentication');
var cardsRouter = require('./routes/cards');

const jwtKey = config.get('secret.key');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressJwt({secret: jwtKey, algorithms: ['HS256']}).unless({path: ["/signin", "/signup", "/"]}));

//Routes
app.use('/', authenticationRouter);
app.use('/users', usersRouter);
app.use('/cards', cardsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;