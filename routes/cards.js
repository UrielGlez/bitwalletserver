const express = require('express');
const controller = require('../controllers/cards');

const router = express.Router();

// CRUD => http methods

router.post('/', controller.create);

router.get('/index/:id', controller.index);

router.get('/:userID/:typeCard?', controller.list);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;
